### 1: Install package

```
git clone https://gitlab.com/ngocquy020196/dotfiles-evim
```

```bash
$ cd dotfiles-evim
```

```bash
$ chmod +x install.sh
```


```bash
$ sudo ./install.sh --linux
```

### 2: Link file 

```bash
$ ./install.sh --link
```

### 3: Set ZSH default bash

```bash
$ sudo vim ~/.bashrc
```

add top file
```
$ exec zsh
```