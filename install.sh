#!/bin/bash
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

function is_installed() {
    # set to 1 initially
    local return_=1
    # set to 0 if not found
    type $1 >/dev/null 2>&1 || { local return_=0; }
    # return
    echo "$return_"
}

function install_linux() {
    echo $OSTYPE
    if [ $(whoami) != root ]; then
        echo Please run this script as root or using sudo
        exit
    fi

    echo "Vim Plug Install"

    if [ "$(is_installed curl)" == "0" ]; then
        echo "Installing curl"
        apt-get install curl -y
    fi

    if [ "$(is_installed git)" == "0" ]; then
        echo "Installing git"
        apt-get install git -y
    fi

    if [ "$(is_installed node)" == "0" ]; then
        echo "Installing Node"
        curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
        apt-get install nodejs -y
    fi

    if [ "$(is_installed nvim)" == "0" ]; then
        echo "Install neovim"
        apt-get install neovim -y
        if [ "$(is_installed pip3)" == "1" ]; then
            pip3 install neovim --upgrade
        fi
    fi

    if [ "$(is_installed zsh)" == "0" ]; then
        echo "Installing zsh"
        apt-get install zsh -y
    fi

    if [ "$(is_installed ag)" == "0" ]; then
        echo "Installing The silver searcher"
        apt-get install silversearcher-ag -y
    fi

    if [ "$(is_installed tmux)" == "0" ]; then
        echo "Installing tmux"
        apt-get install tmux -y
        echo "Installing tmux-plugin-manager"
        git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    fi

    if [ "$(is_installed fzf)" == "0" ]; then
        echo "Installing fzf"
        git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
        source ~/.bashrc # bash
        ~/.fzf/install
    fi
}

function link_dotfiles() {
    if [ $(whoami) == root ]; then
        echo Please run this script as normal or user
        exit
    fi

    rm -rf $(pwd)/bundle
    rm -rf ~/.config/nvim
    rm -rf ~/.zshrc
    rm -rf ~/.tmux.conf
    rm -rf ~/.oh-my-zsh

    echo "Installing oh-my-zsh"
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

    if [ ! -d "~/.oh-my-zsh/plugins/zsh-autosuggestions" ]; then
        echo "Installing zsh-autosuggestions"
        git clone git://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/plugins/zsh-autosuggestions
    fi

    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

    ln -s $(pwd) ~/.config/nvim

    rm -rf ~/.zshrc
    ln -s $(pwd)/tmux.conf ~/.tmux.conf
    ln -s $(pwd)/oh-my-zsh/themes/dracula.zsh-theme ~/.oh-my-zsh/themes/dracula.zsh-theme
    ln -s $(pwd)/zshrc ~/.zshrc

    nvim +PlugInstall

    echo "Install success"

}

while test $# -gt 0; do
    case "$1" in
    --help)
        echo "Help"
        exit
        ;;
    --linux)
        install_linux
        zsh
        source ~/.zshrc
        exit
        ;;
    --link)
        link_dotfiles
        exit
        ;;
    esac

    shift
done
